(function() {
    'use strict';

    angular
        .module('fishMogulApp')
        .config(routes);

    function routes($routeProvider) {
        $routeProvider
            .when('/fishermen',
                {
                    templateUrl: 'templates/fishermen.html',
                    controller: 'FishermenController',
                    controllerAs: 'fishermenCtrl'
                })
            .otherwise({
                redirectTo: '/'
            });
    }

})();