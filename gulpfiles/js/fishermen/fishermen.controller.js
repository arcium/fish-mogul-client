(function() {
    'use strict';

    angular
        .module('fishMogulApp')
        .controller('FishermenController', FishermenController);

    FishermenController.$inject = ['fishermenData'];

    function FishermenController(fishermenData) {
        var vm = this;

        vm.sortType = 'price';
        vm.sortReverse = true;
        vm.searchTerm = '';

        activate();

        function activate() {
            return getData();
        }

        function getData() {
            return fishermenData.getData()
                .then(function(data) {
                    vm.data = data;
                    return vm.data;
                })
        }

        vm.sortTable = function(sortType) {
            var tableSortLinks = document.getElementsByClassName('table-sort-link');
            var tableSortLink = document.getElementById(sortType);
            var className;
            for (var i = 0; i < (tableSortLinks.length); i++) {
                tableSortLinks[i].className = 'table-sort-link';
            }
            tableSortLinks.classname = 'table-sort-link';
            if (vm.sortReverse == true) {
                className = 'active asc';
            } else {
                className = 'active desc';
            }
            tableSortLink.className += ' ' + className;
            vm.sortType = sortType;
            vm.sortReverse = !vm.sortReverse;
        }
    }

})();