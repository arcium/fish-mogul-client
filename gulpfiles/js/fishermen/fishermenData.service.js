(function() {
    'use strict';

    angular
        .module('fishMogulApp')
        .factory('fishermenData', fishermenData);

    fishermenData.$inject = ['$http'];

    function fishermenData($http) {
        var service = {
            getData: getData
        };

        return service;

        function getData() {
            //return $http.get('http://exquisite-chamois-7128.vagrantshare.com/fisherman/retrieve')
            return $http.get('data/fishermen.json')
                .then(success)
                .catch(failed);

            function success(response) {
                return response.data.data;
            }

            function failed(error) {
                console.log(error);
            }
        }
    }

})();