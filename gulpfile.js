var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    minify = require('gulp-cssnano'),
    autoprefixer = require('gulp-autoprefixer'),
    cmq = require('gulp-combine-mq'),
    watch = require('gulp-watch');

gulp.task('app', function() {
    var files = ['gulpfiles/js/app/fishMogul.module.js',
                 'gulpfiles/js/app/fishMogul.routes.js'];
    return gulp.src(files)
        .pipe(concat('app.js'))
        .pipe(gulp.dest('js'));
});

gulp.task('fishermen', function() {
    var files = ['gulpfiles/js/fishermen/fishermenData.service.js',
                 'gulpfiles/js/fishermen/fishermen.controller.js'];
    return gulp.src(files)
        .pipe(concat('fishermen.js'))
        .pipe(gulp.dest('js'));
});

gulp.task('bootstrap', function() {
   return gulp.src('node_modules/bootstrap/dist/css/bootstrap.css')
       .pipe(rename('_bootstrap.scss'))
       .pipe(gulp.dest('gulpfiles/scss/partials'))
});

gulp.task('reset', function() {
    return gulp.src('node_modules/css-reset/reset.css')
        .pipe(rename('_reset.scss'))
        .pipe(gulp.dest('gulpfiles/scss/partials'))
});

gulp.task('scss', function() {
   return gulp.src('gulpfiles/scss/style.scss')
       .pipe(sass())
       .pipe(autoprefixer({
           browsers: ['last 2 versions'],
           cascade: false
       }))
       .pipe(cmq({
           beautify: true
       }))
       .pipe(sass())
       .pipe(gulp.dest('css'))
       .pipe(minify())
       .pipe(rename({
           suffix: '.min'
       }))
       .pipe(gulp.dest('css'));
});

gulp.task('watch', function() {
    gulp.watch('gulpfiles/js/**', ['app', 'fishermen']);
    gulp.watch('gulpfiles/scss/**', ['scss']);
});

gulp.task('default', function () {
    gulp.run('watch');
});